package org.example;

public class Vehicle {
    private int total;
    public void type(){
        System.out.println("Type: Bus");
    }
    public void fuel(){
        System.out.println("fuel: Gasoline");
    }
    public void setTotal(int newTotal){
        this.total = newTotal;
    }

    public int getTotal() {
        return this.total;
    }
}
