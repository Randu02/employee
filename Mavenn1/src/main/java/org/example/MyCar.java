package org.example;

public class MyCar extends Vehicle {
        public void detail(){
            MyCar myNewCar = new MyCar();
            myNewCar.type();
            super.fuel();
            myNewCar.fuel();
            myNewCar.color();
            myNewCar.setTotal(10);
            int myNewCarTotal = myNewCar.getTotal();
            System.out.println("Total car: "+ myNewCarTotal);
        }
    public void fuel(){
        System.out.println("Fuel: Solar");
    }
    public void color(){
        System.out.println("Color: Black");
    }
    public static void firstRun(){
        System.out.println("Main first run");
    }
}
