package org.example;

import org.example.test.Car;

/**
 * Hello world!
 *
 */
public class App extends Vehicle
{
    public static void main( String[] args )
    {
        firstRun();
        System.out.println( "Hello World!" );
        Vehicle myCar = new Vehicle();
        myCar.type();

//        App myNewCar = new  App();
//        myNewCar.type();
//        myNewCar.fuel();
//        myNewCar.color();
//        myNewCar.setTotal(10);
//        int myNewCarTotal = myNewCar.getTotal();
//        System.out.println("Total car: "+ myNewCarTotal);

        MyCar myNewCar = new  MyCar();
        myNewCar.detail();
        Car myDetail = new Car();
        myDetail.transmission();

    }

    public void fuel(){
        System.out.println("Fuel: Solar");
    }
    public void color(){
        System.out.println("Color: Black");
    }
    public static void firstRun(){
        System.out.println("Main first run");
    }

}
